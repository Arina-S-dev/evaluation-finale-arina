
INSERT INTO users (name, password)
VALUES ('arina', '$2a$16$xYLXmUaawBWO6tpYDUvPQOWpbfnuRfNnxZd1jAyufYaCD9diuNsXi');

INSERT INTO projects (name, description, user_name)
VALUES
('Projet de développement du site web', 'Ce projet consiste à développer un nouveau site web pour notre entreprise, avec un design moderne et des fonctionnalités améliorées.', 'arina'),
('Projet de refonte de l''application mobile', 'Nous devons mettre à jour notre application mobile existante avec de nouvelles fonctionnalités et une meilleure interface utilisateur.', 'arina'),
('Projet de lancement de produit', 'Nous préparons le lancement de notre nouveau produit et devons coordonner les efforts de marketing, de développement et de production.', 'arina');

INSERT INTO tickets (title, content, project_id)
VALUES
('Créer la page d''accueil du site web', 'Design accrocheur avec des images et des vidéos pour attirer l''attention des visiteurs.', 1),
('Ajouter une fonctionnalité de chat', 'Permettre aux utilisateurs de discuter en temps réel avec d''autres utilisateurs de l''application.', 2),
('Organiser un événement de lancement', 'Trouver un lieu, inviter des journalistes et des influenceurs, et préparer une présentation.', 3),
('Tester la fonctionnalité de paiement en ligne', 'Vérifier que les paiements en ligne fonctionnent correctement et que les transactions sont sécurisées.', 1),
('Améliorer la vitesse de chargement de l''app', 'Les utilisateurs se plaignent que l''application est lente, nous devons donc optimiser le code pour améliorer les performances.', 2);









INSERT INTO tickets (title, content, project_id)
VALUES ('tâche 1', 'ma super tâche1', 1),
('tâche 2', 'ma super tâche2', 2),
('tâche 3', 'ma super tâche3', 3);

