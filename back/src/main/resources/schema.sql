DROP table IF EXISTS users, projects, tickets;

CREATE TABLE IF NOT EXISTS users
(
    id INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name VARCHAR(50) NOT NULL UNIQUE,
    password  VARCHAR(1000) NOT NULL
);

CREATE TABLE IF NOT EXISTS projects
(
    id INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(500) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    user_name VARCHAR(255) NOT NULL REFERENCES users(name)
);

CREATE TABLE IF NOT EXISTS tickets
(
    id INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    title VARCHAR(50) NOT NULL,
    content  VARCHAR(1000) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    status VARCHAR(50) NOT NULL DEFAULT 'TODO',
    project_id INTEGER NOT NULL REFERENCES projects(id)
);



