package com.zenika.zira.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Entity
@Table(name = "tickets")
@Getter
@Setter
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "content", nullable = false)
    @Size(max = 1000)
    private String content;

    @Column(columnDefinition = "TIMESTAMP NOT NULL DEFAULT NOW()")
    private Timestamp createdAt;

    @Column(name = "status", nullable = false, columnDefinition = "VARCHAR(50) NOT NULL DEFAULT 'TODO'")
    private String status;

    @Column(name = "project_id", nullable = false)
    private Integer projectId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @JsonIgnore
    private Project project;

    public Ticket() {
    }
}
