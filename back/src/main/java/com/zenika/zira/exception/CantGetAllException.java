package com.zenika.zira.exception;

public class CantGetAllException extends Exception {
    public CantGetAllException(String message) {
        super(message);
    }
}
