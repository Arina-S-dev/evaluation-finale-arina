package com.zenika.zira.exception;

public class CantCreateNewException extends Exception {
    public CantCreateNewException(String message) {
        super(message);
    }
}
