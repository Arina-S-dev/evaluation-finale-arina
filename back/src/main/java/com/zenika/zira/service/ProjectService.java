package com.zenika.zira.service;

import com.zenika.zira.domain.Project;
import com.zenika.zira.exception.CantCreateNewException;
import com.zenika.zira.exception.CantGetAllException;
import com.zenika.zira.exception.NotFoundException;
import com.zenika.zira.repository.ProjectRepository;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {

        this.projectRepository = projectRepository;
    }

    public List<Project> getAlls()  throws CantGetAllException {
        if(projectRepository.findAll().size() == 0) {
            throw  new CantGetAllException("La liste de projets est vide");
        }
        return projectRepository.findAllByOrderByCreatedAtDesc();
    }

    public Project getById(Integer id)  throws NotFoundException {
        if(id == 0) {
            throw  new NotFoundException("Impossible de trouver le projet");
        }
        return projectRepository.findById(id).get();
    }

    public Project create(Project project) throws CantCreateNewException {
        if(project == null) {
            throw  new CantCreateNewException("Impossible de créer un nouveau projet");
        }
        project.setCreatedAt(Timestamp.valueOf(LocalDateTime.now()));
        projectRepository.save(project);
        return project;
    }

    public void delete(Integer id) throws NotFoundException {
        if(id == null) {
            throw  new NotFoundException("Impossible de supprimer le projet");
        }
        projectRepository.delete(getById(id));
    }

    public Project update(Integer id, Project projectDetails) throws NotFoundException {
        if(id == null) {
            throw  new NotFoundException("Impossible de trouver le projet");
        }
        Project projectToUpdate = projectRepository.findById(id).get();
        projectToUpdate.setName(projectDetails.getName());
        projectToUpdate.setDescription(projectDetails.getDescription());
        return projectRepository.save(projectToUpdate);
    }
}
