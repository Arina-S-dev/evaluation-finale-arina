package com.zenika.zira.service;

import com.zenika.zira.domain.Project;
import com.zenika.zira.domain.Ticket;
import com.zenika.zira.exception.CantCreateNewException;
import com.zenika.zira.exception.CantGetAllException;
import com.zenika.zira.exception.NotFoundException;
import com.zenika.zira.repository.ProjectRepository;
import com.zenika.zira.repository.TicketRepository;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class TicketService {

    private final TicketRepository ticketRepository;

    public TicketService(TicketRepository ticketRepository) {

        this.ticketRepository = ticketRepository;
    }

    public List<Ticket> getAlls()  throws CantGetAllException {
        if(ticketRepository.findAll().size() == 0) {
            throw  new CantGetAllException("La liste de tickets est vide");
        }
        return ticketRepository.findAll();
    }

    public Ticket getById(Integer id)  throws NotFoundException {
        if(id == 0) {
            throw  new NotFoundException("Impossible de trouver le ticket");
        }
        return ticketRepository.findById(id).get();
    }

    public void create(Ticket ticket) throws CantCreateNewException {
        if(ticket == null) {
            throw  new CantCreateNewException("Impossible de créer un nouveau ticket");
        }
        ticket.setCreatedAt(Timestamp.valueOf(LocalDateTime.now()));
        ticketRepository.save(ticket);
    }

    public void delete(Integer id) throws NotFoundException {
        if(id == null) {
            throw  new NotFoundException("Impossible de supprimer le ticket");
        }
        ticketRepository.delete(getById(id));
    }

    public Ticket update(Integer id, Ticket ticketDetails) throws NotFoundException {
        if(id == null) {
            throw  new NotFoundException("Impossible de trouver le ticket");
        }
        Ticket ticketToUpdate = ticketRepository.findById(id).get();
        ticketToUpdate.setTitle(ticketDetails.getTitle());
        ticketToUpdate.setContent(ticketDetails.getContent());
        ticketToUpdate.setStatus(ticketDetails.getStatus());
        return ticketRepository.save(ticketToUpdate);
    }
}
