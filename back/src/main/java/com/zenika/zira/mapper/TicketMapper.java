package com.zenika.zira.mapper;

import com.zenika.zira.controller.TicketReadRepresentation;
import com.zenika.zira.domain.Ticket;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface TicketMapper {
    Ticket map(TicketReadRepresentation ticketReadRepresentation);
}
