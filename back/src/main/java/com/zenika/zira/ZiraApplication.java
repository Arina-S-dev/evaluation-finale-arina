package com.zenika.zira;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class ZiraApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZiraApplication.class, args);
    }
}