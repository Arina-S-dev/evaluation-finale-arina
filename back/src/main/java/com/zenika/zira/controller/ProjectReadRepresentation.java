package com.zenika.zira.controller;


import java.util.List;


public record ProjectReadRepresentation (
    long id,
    String name,

    String description,
    List<String> tickets

) {}

