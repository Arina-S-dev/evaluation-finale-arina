package com.zenika.zira.controller;

import com.zenika.zira.domain.Project;
import com.zenika.zira.exception.CantCreateNewException;
import com.zenika.zira.exception.CantGetAllException;
import com.zenika.zira.exception.NotFoundException;
import com.zenika.zira.service.ProjectService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectController {

    private final ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping
    public ResponseEntity<?> getAlls() throws CantGetAllException {
        try {
            List<Project> projects = projectService.getAlls();
            return (ResponseEntity.ok(projects));
        } catch (CantGetAllException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            String errorMessage = "Une erreur s'est produite lors de la récupération des projets : " + e.getMessage();
            return ResponseEntity.badRequest().body(errorMessage);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable(value = "id") Integer id) throws NotFoundException {
        try {
            Project project = projectService.getById(id);
            return (ResponseEntity.ok(project));
        } catch (NotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            String errorMessage = "Une erreur s'est produite lors de la récupération des projets : " + e.getMessage();
            return ResponseEntity.badRequest().body(errorMessage);
        }
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody @Validated Project project) throws  CantCreateNewException{
        try {
            Project newProject = projectService.create(project);
            return ResponseEntity.ok(newProject);
        } catch (CantCreateNewException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Une erreur s'est produite côté serveur");
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable(value = "id") Integer id) throws NotFoundException {
        try {
            projectService.delete(id);
            return ResponseEntity.ok().build();
        } catch (NotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Une erreur s'est produite côté serveur");
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable(value = "id") Integer id, @Valid @RequestBody Project projectDetails)
            throws NotFoundException {
        try {
            projectService.update(id, projectDetails);
            return ResponseEntity.ok("Le projet a bien été modifié");
        } catch (NotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Une erreur s'est produite côté serveur");
        }

    }

}
