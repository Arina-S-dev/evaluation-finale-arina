package com.zenika.zira.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
// Swagger with basic auth
@SecurityRequirement(name = "basicAuth")
public class UserController {

    @GetMapping("/me")
    public ResponseEntity<User> getMe() {
        User myInMemoryUserFromUserDetailsService = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return ResponseEntity.ok(myInMemoryUserFromUserDetailsService);
    }
}