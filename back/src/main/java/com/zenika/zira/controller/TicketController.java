package com.zenika.zira.controller;

import com.zenika.zira.domain.Ticket;
import com.zenika.zira.exception.CantCreateNewException;
import com.zenika.zira.exception.CantGetAllException;
import com.zenika.zira.exception.NotFoundException;
import com.zenika.zira.mapper.TicketMapper;
import com.zenika.zira.service.TicketService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tickets")
public class TicketController {

    private final TicketService ticketService;

    private final TicketMapper ticketMapper;

    public TicketController(TicketService ticketService, TicketMapper ticketMapper) {
        this.ticketService = ticketService;
        this.ticketMapper = ticketMapper;
    }

    @GetMapping
    public ResponseEntity<?> getAlls() throws CantGetAllException {
        try {
            List<Ticket> tickets = ticketService.getAlls();
            return (ResponseEntity.ok(tickets));
        } catch (CantGetAllException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            String errorMessage = "Une erreur s'est produite lors de la récupération des tickets : " + e.getMessage();
            return ResponseEntity.badRequest().body(errorMessage);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable(value = "id") Integer id) throws NotFoundException {
        try {
            Ticket ticket = ticketService.getById(id);
            return (ResponseEntity.ok(ticket));
        } catch (NotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            String errorMessage = "Une erreur s'est produite lors de la récupération des tickets : " + e.getMessage();
            return ResponseEntity.badRequest().body(errorMessage);
        }
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody @Validated TicketReadRepresentation ticketReadRepresentation) {
        try {
            Ticket ticket = ticketMapper.map(ticketReadRepresentation);
            ticketService.create(ticket);
            return ResponseEntity.ok(ticket);
        } catch (CantCreateNewException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Une erreur s'est produite côté serveur");
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable(value = "id") Integer id) throws NotFoundException {
        try {
            ticketService.delete(id);
            return ResponseEntity.ok().build();
        } catch (NotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Une erreur s'est produite côté serveur");
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable(value = "id") Integer id, @Valid @RequestBody Ticket ticketDetails)
            throws NotFoundException {
        try {
            ticketService.update(id, ticketDetails);
            return ResponseEntity.ok("Le ticket a bien été modifié");
        } catch (NotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Une erreur s'est produite côté serveur");
        }
    }
}
