package com.zenika.zira.controller;

import com.zenika.zira.domain.Project;

public record TicketReadRepresentation(

        Integer projectId,
        String title,

        String content,
        String status

) {
}
