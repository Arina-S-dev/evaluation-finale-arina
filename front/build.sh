#!/bin/bash

set -e

export NODE_OPTIONS=--openssl-legacy-provider

based_tag_name=${CI_REGISTRY_IMAGE:-arina/zira}


npm install
#npm run test
npm run build
docker build -t "$based_tag_name/front:latest" .
