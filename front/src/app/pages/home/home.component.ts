import { Component, Inject, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(public projectService: ProjectService) {}

  ngOnInit(): void {
    this.projectService.getProjects().subscribe()
  }
}

