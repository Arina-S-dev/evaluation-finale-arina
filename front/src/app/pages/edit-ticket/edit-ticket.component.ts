import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Ticket } from 'src/app/models/ticket';
import { TicketDto } from 'src/app/models/ticket-dto';
import { ProjectService } from 'src/app/services/project.service';
import { TicketService } from 'src/app/services/ticket.service';


@Component({
  selector: 'app-edit-ticket',
  templateUrl: './edit-ticket.component.html',
  styleUrls: ['./edit-ticket.component.scss']
})
export class EditTicketComponent implements OnInit {

  ticketFormUpdate!: FormGroup;

  ticketToUpdate!: Ticket;

  projectId!: number | null;

  ticketId!: number | null;

constructor(private formBuilder: FormBuilder,private ticketService: TicketService, private router: Router, private activatedRoute: ActivatedRoute, private projectService: ProjectService,
  ) { }

  ngOnInit(): void {
    this.projectId = Number(this.activatedRoute.snapshot.paramMap.get('projectId'));
    this.ticketId = Number(this.activatedRoute.snapshot.paramMap.get('ticketId'));
    this.ticketService.findById(this.ticketId).subscribe((ticket) => {
        if(ticket) {
          this.ticketFormUpdate = this.formBuilder.group({
            title: [ticket.title, Validators.required],
            content: [ticket.content, Validators.required],
            status: [ticket.status, Validators.required]
          });
        }
    });
  }

  submit() {
    const formValues = this.ticketFormUpdate.value;
    if(this.ticketFormUpdate.valid && formValues.title !== null && formValues.content !== null ){
      const ticketValues = {
        projectId : this.projectId,
        ...this.ticketFormUpdate.value,
      } as TicketDto;
      console.log(ticketValues);
      this.ticketService.createTicket(ticketValues).subscribe( () =>
          this.router.navigateByUrl("/projects/" + this.projectId));
        }
    }
}
