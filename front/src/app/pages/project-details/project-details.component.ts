import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Project } from 'src/app/models/project';
import { switchMap, tap } from 'rxjs/operators';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss']
})
export class ProjectDetailsComponent implements OnInit {

  public project$!: Observable<Project | undefined>

  constructor(
    private activatedRoute: ActivatedRoute,
    private projectService: ProjectService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.project$ = this.activatedRoute.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.projectService.getById(params.get('id') as string)
      ),
      tap((project: Project | undefined) => {
        if (!project) {
          this.router.navigateByUrl('/')
        }
      })
    )
  }

  deleteProject(project: Project) {
    this.projectService.deleteProject(project.id).subscribe(()=> {
      this.router.navigate(['/'])
    })
    }
  }


