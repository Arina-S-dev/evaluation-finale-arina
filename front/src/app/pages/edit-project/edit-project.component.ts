import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Project } from 'src/app/models/project';
import { ProjectDto } from 'src/app/models/project-dto';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.scss']
})
export class EditProjectComponent implements OnInit {

  projectFormToUpdate!: FormGroup;

  projectId!: number;

  constructor(private formBuilder: FormBuilder, private projectService: ProjectService, private router: Router, private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.projectId = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.projectService.getById(this.projectId).subscribe((project) => {
      if (project) {
        this.projectFormToUpdate = this.formBuilder.group({
          name: [project.name, Validators.required],
          description: [project.description, Validators.required]
        });
      }
    })
  };

  submit() {
    const formValues = this.projectFormToUpdate.value;
    console.log(formValues);
    if (this.projectFormToUpdate.valid && formValues.name !== null && formValues.description !== null) {
      const projectValues = this.projectFormToUpdate.value as ProjectDto;
      this.projectService.createProject(projectValues).subscribe(
        (newProject: Project) => {
          this.router.navigateByUrl("/projects/" + newProject.id);
        }
      )
    }
  }
}
