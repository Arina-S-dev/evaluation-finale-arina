import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { HomeComponent } from './pages/home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { ProjectListComponent } from './components/project-list/project-list.component';
import { ProjectCardComponent } from './components/project-card/project-card.component';
import { ProjectTicketsComponent } from './components/project-tickets/project-tickets.component';
import { ProjectDetailsComponent } from './pages/project-details/project-details.component';
import { AddProjectComponent } from './pages/add-project/add-project.component';
import { ProjectFormComponent } from './components/project-form/project-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TicketFormComponent } from './components/ticket-form/ticket-form.component';
import { AddTicketComponent } from './pages/add-ticket/add-ticket.component';
import { EditTicketComponent } from './pages/edit-ticket/edit-ticket.component';
import { EditProjectComponent } from './pages/edit-project/edit-project.component';
import { LoginComponent } from './pages/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NavigationComponent,
    HomeComponent,
    ProjectListComponent,
    ProjectCardComponent,
    ProjectTicketsComponent,
    ProjectDetailsComponent,
    AddProjectComponent,
    ProjectFormComponent,
    TicketFormComponent,
    AddTicketComponent,
    EditTicketComponent,
    EditProjectComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
