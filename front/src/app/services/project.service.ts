import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Project } from '../models/project';
import {
  BehaviorSubject,
  Observable,
} from 'rxjs';
import {
  tap
} from 'rxjs/operators';
import { ProjectDto } from '../models/project-dto';



@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  public projects$: BehaviorSubject<Project[]> = new BehaviorSubject<Project[]>([])

  private projectsUrl = '/api/projects';  // URL to web api

  constructor(private http: HttpClient) {
   }

   getProjects(): Observable<Project[]> {
    return this.http.get<Project[]>(this.projectsUrl).pipe(
        tap((projects) => this.projects$.next(projects))
      )
    }

    getById(id: number | string): Observable<Project | undefined> {
      return this.http.get<Project>(`${this.projectsUrl}/${id}`)
    }

    deleteProject(id: number) {
      return this.http.delete<void>(`${this.projectsUrl}/${id}`).pipe(
        tap(() => {
          const projects = this.projects$.getValue();
          const filteredProjects = projects.filter(project => project.id !== id);
          this.projects$.next(filteredProjects);
        })
      )
    }

    createProject(project: ProjectDto): Observable<Project> {
      return this.http.post<Project>(this.projectsUrl, project)
     }
}
