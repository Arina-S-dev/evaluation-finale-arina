import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Ticket } from 'src/app/models/ticket';
import { TicketService } from 'src/app/services/ticket.service';

@Component({
  selector: 'app-project-tickets',
  templateUrl: './project-tickets.component.html',
  styleUrls: ['./project-tickets.component.scss']
})
export class ProjectTicketsComponent {

  @Input()
  public tickets: Ticket[] = []
  @Input()
  public projectId!: number;

  constructor(private ticketService: TicketService, private router: Router
    ) { }


  deleteTicket(ticket: Ticket) {
    this.ticketService.deleteTicket(ticket.id).subscribe(()=> {
      const currentUrl = this.router.url;
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this.router.navigate([currentUrl]);
      });
    })
    }
  }
  
