import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Project } from 'src/app/models/project';
import { ProjectDto } from 'src/app/models/project-dto';
import { AuthentificationService } from 'src/app/services/authentification.service';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-project-form',
  templateUrl: './project-form.component.html',
  styleUrls: ['./project-form.component.scss']
})
export class ProjectFormComponent implements OnInit {

  projectForm!: FormGroup;

  username! : string;

  constructor(private formBuilder: FormBuilder, private projectService: ProjectService, private router: Router, private authentificationService: AuthentificationService
  ) { }

  ngOnInit(): void {
    this.projectForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required]
    });
    const currentUser = JSON.parse(localStorage.getItem('currentUser')!);
    this.username = currentUser.username;
  }

  submit() {
    

    const formValues = this.projectForm.value;
    console.log(formValues);
    if (this.projectForm.valid && formValues.name !== null && formValues.description !== null) {
      const projectValues = this.projectForm.value as ProjectDto;
      this.projectService.createProject({
        ...projectValues,
        username: this.username}).subscribe(
        (newProject: Project) => {
          this.router.navigateByUrl("/projects/" + newProject.id);
        }
      )
    }
  }
  //console.log(this.authentificationService.LOCAL_STORAGE_KEY);
  //console.log(this.authentificationService.getCurrentUserBasicAuthentication());

}
