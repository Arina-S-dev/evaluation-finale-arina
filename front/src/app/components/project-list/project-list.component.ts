import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from 'src/app/models/project';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit {

  @Input('projects')
  public projects$?: Observable<Project[]>


  constructor() { }

  ngOnInit(): void {
  }

  trackProjectFn(_: number, item: Project) {
    return item.id
  }


}
