import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Project } from 'src/app/models/project';
import { ProjectDto } from 'src/app/models/project-dto';
import { TicketDto } from 'src/app/models/ticket-dto';
import { ProjectService } from 'src/app/services/project.service';
import { TicketService } from 'src/app/services/ticket.service';

@Component({
  selector: 'app-ticket-form',
  templateUrl: './ticket-form.component.html',
  styleUrls: ['./ticket-form.component.scss']
})
export class TicketFormComponent implements OnInit {

ticketForm!: FormGroup;

projectId!: number | null;

constructor(private formBuilder: FormBuilder,private ticketService: TicketService, private router: Router, private activatedRoute: ActivatedRoute, private projectService: ProjectService,
  ) { }

  ngOnInit(): void {
    this.ticketForm = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required],
      status: ['TODO', Validators.required]
    });
    this.projectId = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    console.log(this.projectId)
  }

  submit() {
    const formValues = this.ticketForm.value;
    if(this.ticketForm.valid && formValues.title !== null && formValues.content !== null ){
      const ticketValues = {
        projectId : this.projectId,
        ...this.ticketForm.value,
      } as TicketDto;
      console.log(ticketValues);
      this.ticketService.createTicket(ticketValues).subscribe( () =>
          this.router.navigateByUrl("/projects/" + this.projectId));
        }
    }
  }
