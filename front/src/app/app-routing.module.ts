import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ProjectDetailsComponent } from './pages/project-details/project-details.component';
import { AddProjectComponent } from './pages/add-project/add-project.component';
import { AddTicketComponent } from './pages/add-ticket/add-ticket.component';
import { EditTicketComponent } from './pages/edit-ticket/edit-ticket.component';
import { EditProjectComponent } from './pages/edit-project/edit-project.component';
import { LoginComponent } from './pages/login/login.component';
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import { BasicAuthInterceptor } from './helpers/basic-auth.interceptor';
import { ErrorInterceptor } from './helpers/error.interceptor';

const routes: Routes = [
  {  path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'projects/:id', component: ProjectDetailsComponent },
  { path: 'add-new-project', component: AddProjectComponent },
  { path: 'projects/:id/add-new-ticket', component: AddTicketComponent },
  { path: 'projects/:id/edit', component: EditProjectComponent },
  { path: 'projects/:projectId/edit-ticket/:ticketId', component: EditTicketComponent },
  { path: 'login', component: LoginComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor,
      multi: true
    }
  ]
})
export class AppRoutingModule { }
