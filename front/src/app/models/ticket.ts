import { Timestamp } from "rxjs"

export interface Ticket {
    id: number,
    title: string,
    content: string,
    createdAt: Date,
    status: string
}
