export interface ProjectDto {
    name: string,
    description: string,
    username: string
  }
