import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Router} from "@angular/router";
import { AuthentificationService } from '../services/authentification.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private authentificationService: AuthentificationService, private router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      if (err.status === 401) {
        // auto logout if 401 response returned from backend
        this.authentificationService.logout();
        this.router.navigate(['/login'],{ queryParams: { returnUrl: this.router.routerState.snapshot.url }});
      }
      return throwError(err);
    }))
  }
}