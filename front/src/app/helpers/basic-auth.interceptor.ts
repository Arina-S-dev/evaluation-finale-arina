import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import { AuthentificationService } from '../services/authentification.service';

@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {
  constructor(private readonly authentificationService: AuthentificationService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const auth = this.authentificationService.getCurrentUserBasicAuthentication()
    // add authorization header with basic auth credentials if available
    if (auth) {
      request = request.clone({
        setHeaders: {
          Authorization: auth
        }
      });
    }

    return next.handle(request);
  }
}